<?php
/**
 * 定数
 * @var unknown
 */

/**
 * 設定ファイルを取得する。
 * @return array
 */
function getConfig(){
    $result = array();
    $result = parse_ini_file(CONFIG_FILE);

    return $result;
}
/**
 * セッションの保存先取得
 * @return string
 */
function getSessionDir()
{
  $config = null;
  $config = getConfig();

  return $config['session_dir'];
}