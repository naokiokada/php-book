<?php
/*
 * DBのデータ取得フェッチモード
 * を連想配列に固定する。
 */
define('FETCH_MODE', PDO::FETCH_ASSOC);

/**
 * DBのオブジェクトを指定する。
 * @param string $dns
 * @param string $user
 * @param string $passwd
 * @return PDO
 */
function getConnection(string $dns, string $user, string $passwd)
{
    $db = null;

    try{
        $db = new PDO($dns, $user, $passwd);
    }catch(PDOException $e){
        echo('エラー：' . $e->getMessage());
    }
    return $db;
}
/**
 * DBに接続するオプション
 * dnsを取得する。
 * @param string $type
 * @param string $dbname
 * @param string $host
 * @param integer $port
 * @return string
 */
function getDNSvalue(string $type, string $dbname, string $host, string $port)
{
    $dns = '';

    switch($type){
        case 'pgsql':
            $dns = $type . ':dbname=' . $dbname .
            ' host=' . $host . ' port='.$port;
            break;
        default:
            $dns = $type . ':dbname=' . $dbname .
            ' host=' . $host . ' port='.$port;
      break;
  }

  return $dns;
}
/**
 * SQLを実行する
 * @param string $sql
 * @param array $bind
 * @param PDOObject $db
 * @return unknown
 */
function executeSQL(string $sql, array $bind = array(), $db)
{
    $stmt = false;
    try{
        $stmt = $db->prepare($sql);
        $stmt->execute($bind);
    }catch(PDOException $e){
        echo('エラー：' . $e->getMessage());
    }

    return $stmt;
}

/**
 * SQL実行結果をすべて連想配列で返す。
 * @param PDOObject $stmt
 * @return array
 */
function fetchAll($stmt)
{
    $result = array();
    $result = $stmt->fetchAll();

    return $result;
}
/**
 * SQL実行結果で1行のみの結果を返す。
 * @param PDOObject $stmt
 * @return array
 */
function fetchRow($stmt){
    $result = array();
    $result = $stmt->fetch(FETCH_MODE);

    return $result;
}