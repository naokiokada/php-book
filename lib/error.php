<?php
/**
 * エラーを扱うphp
 */

/**
 * エラーコードを管理する。
 * @return string[]
 */
function getErrorCode()
{
    $errList = array(
            '1'=>'ID、パスワードが正しくありません。',
            '2'=>'認証されていません。再度ログイン画面から認証して下さい。'
    );

    return $errList;
}