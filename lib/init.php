<?php
/**
 * 処理全体で利用する定数定義
 * @var unknown
 */
define('DS', DIRECTORY_SEPARATOR);
//プロジェクトの配置場所を指定する。
define('APP_DIR', '/var/www/php-book');
define('CONFIG_FILE', APP_DIR . DS . 'config' . DS . 'app.ini');

/**
 * ライブラリとして利用するPHPファイルの読み込み
 */
require_once dirname(__FILE__) . '/config.php';
require_once dirname(__FILE__) . '/session.php';
require_once dirname(__FILE__) . '/db.php';
require_once dirname(__FILE__) . '/error.php';