<?php
/**
 * セッション関係を扱う
 * 関数ライブラリ
 */

/**
 * セッション初期処理
 * @param string $dir
 */
function initSession(string $dir)
{
    if($dir){
        setSavePath($dir);
    }
    session_start();
}
/**
 * セッション保存先設定
 * プロジェクト配下のsessionディレクトリに
 * 保存する。
 * @param string $dir
 */
function setSavePath(string $dir)
{
    $dir = APP_DIR . DS .$dir;
    session_save_path($dir);
}
/**
 * 認証セッションの保存
 * @param string $key
 * @param string $userid
 */
function setSessionAuth(string $key, string $userid)
{
    $_SESSION[$key] = $userid;
}
/**
 * 認証のチェック
 * @param string $key
 * @param string $userid
 * @return boolean
 */
function checkSessionAuth(string $key){
    if(isset($_SESSION[$key])){
        return true;
    }

    return false;
}
/**
 * セッションを閉じる
 * ログアウト時に実行
 */
function destoroySession()
{
    session_destroy();
}