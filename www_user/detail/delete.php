<?php
//他のPHPプログラムファイルの読み込み
require_once '../../lib/init.php';
//定義済みの関数を呼び出し
//設定ファイル情報を読み込む
$config = getConfig();
//セッションの初期処理
initSession($config['SESSION_DIR']);
if(!checkSessionAuth($config['SESSION_NEED_KEY'])){
    //認証されていない
    header('Location: /?err=2');
    exit;
}
if(validate()){
    //長い処理は、改行を入れても大丈夫。
    $dbDNS  = getDNSvalue($config['DB_TYPE'], $config['DB_NAME'],
        $config['DB_HOST'], $config['DB_PORT']);
    //PDO DBというDB操作するモジュールを利用する。
    $db     = getConnection($dbDNS, $config['DB_USER'], $config['DB_PASSWD']);
    //book_tblから削除
    deleteBooktbl();
    //処理したら一覧画面に戻る
    header('Location: /list/');
    exit;
}else{
    //book_idがない場合はエラーを表示
    //下記のhtml
}
/**
 * book_idの有効性確認
 * @return boolean
 */
function validate()
{
    if(!isset($_POST['book_id']) || $_POST['book_id']===''){
        return false;
    }
    return true;
}
/**
 * book_tblの情報を削除する。
 * ※更新
 */
function deleteBooktblWithUpdate()
{
    global $db;

    $sql = "update book_tbl set avail_flg = :avail_flg,
update_time =:update_time,update_id =:update_id
 where book_id =:book_id";

    //deleteをした場合
    //$sql = "delete from book_tbl where book_id =:book_id";

    $bind = array(
        ':avail_flg'  =>'0',
        ':update_time'=>date('YmdHis'),
        ':update_id'  =>$_SESSION['user_id'],
        ':book_id'    =>$_GET['book_id']
    );
    $stmt = executeSQL($sql, $bind, $db);
}
/**
 * 登録済みの本を消す。
 * SQLのdelete版
 * システム的には、あまり良くない処理。
 */
function deleteBooktbl()
{
    global $db;

    $sql = "delete from book_tbl where book_id = :book_id";
    $bind = array(
        ':book_id'=>$_POST['book_id']
    );
    $stmt = executeSQL($sql, $bind, $db);
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>登録エラー</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../statics/css/bootstrap.min.css" rel="stylesheet">
    <link href="../statics/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="../statics/js/jquery-3.1.1.min.js"></script>
    <script src="../statics/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    function goback()
    {
        location.href = '/list/';
    }
    </script>
</head>
<body>
    <form name="fm" id="fm" action="" method="" class="form-horizontal" role="form" enctype="multipart/form-data">
        <div class="container">
            <h3 class="page-header">登録エラー</h3>
            <div class="alert alert-danger" role="alert">すべての登録内容が入力されておりません。
            再度、すべての入力項目を登録下さい。<br />
            一度、一覧ページに戻ります。
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="button" class="btn btn-primary" onClick="goback();">戻る</button>
                </div>
            </div>
        </div>
    </form>
</body>
</html>