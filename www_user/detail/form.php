<?php
//他のPHPプログラムファイルの読み込み
require_once '../../lib/init.php';

//定義済みの関数を呼び出し
$config = getConfig();
//セッションの初期処理
initSession($config['SESSION_DIR']);
if(!checkSessionAuth($config['SESSION_NEED_KEY'])){
	//認証されていない
	header('Location: /?err=2');
	exit;
}
//長い処理は、改行を入れても大丈夫。
$dbDNS  = getDNSvalue($config['DB_TYPE'], $config['DB_NAME'],
		$config['DB_HOST'], $config['DB_PORT']);
//PDO DBというDB操作するモジュールを利用する。
$db     = getConnection($dbDNS, $config['DB_USER'], $config['DB_PASSWD']);
$mode   = getMode();
//DB処理、エラー前情報の取得
$result = null;
if($mode === 'edit'){
    //編集モードだったら、登録済み情報を取得する。
    $result = getBookInfoForEdit();
}
/**
 * フォームのモードを判定する。
 * @return NULL|string
 */
function getMode()
{
    $result = 'add';

    if(isset($_GET['mode'])){
        if($_GET['mode'] ==='edit'){
            $result = 'edit';
        }
    }

    return $result;
}
/**
 * 編集の為にbook_tblから既存
 * 情報を取得する。
 * @return array
 */
function getBookInfoForEdit()
{
    global $db;

    $book_id = null;
    if(isset($_GET['book_id'])){
        $book_id = $_GET['book_id'];
    }
    //ここに登録済みの本情報を取得する。その場合は、book_idが必要。
    $sql = "select book_id,book_name,publish_name,page_num
 from book_tbl where book_id = :book_id";

    $bind = array(':book_id'=>$book_id);

    $result = array();
    $stmt = executeSQL($sql,$bind,$db);
    $result = fetchRow($stmt);

    return $result;
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?php if($mode === 'add'){ ?>
    <title>書籍の登録</title>
<?php }elseif($mode === 'edit'){ ?>
    <title>書籍の編集</title>
<?php }else{} ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../statics/css/bootstrap.min.css" rel="stylesheet">
    <link href="../statics/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="../statics/js/jquery-3.1.1.min.js"></script>
    <script src="../statics/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        function goSave(){
            //ここにJavaSctiptで登録前の値チェックを作る
            var book_name      = document.fm.book_name.value;
            var publish_name = document.fm.publish_name.value;
            var page_num      = document.fm.page_num.value;
            if(!book_name){
                alert('本の名前を入力して下さい。');
                document.fm.book_name.focus();
                return;
            }
            if(!publish_name){
                alert('出版社の名前を入力して下さい。');
                document.fm.publish_name.focus();
                return;
            }
            if(!page_num){
                alert('ページ数を入力して下さい。');
                document.fm.page_num.focus();
                return;
            }
            //alert(document.fm.action);
            //alert(document.fm.method);
            document.fm.submit();
        }
        </script>
</head>
<body>
    <form name="fm" id="fm" action="./save.php" method="post" class="form-horizontal" role="form" enctype="multipart/form-data">
    <input type="hidden" name="book_id" id="book_id" value="<?php echo(isset($result['book_id']) ? $result['book_id'] : ''); ?>">
        <div class="container">
<?php if($mode === 'add'){ ?>
            <h3 class="page-header">書籍の登録</h3>
<?php }elseif($mode === 'edit'){ ?>
            <h3 class="page-header">書籍の編集</h3>
<?php }else{} ?>
            <div class="form-group">
                <label class="control-label col-sm-2 col-lg-2 " for="id_name">書籍名</label>
            <div class=" col-sm-10 col-lg-10 ">
                <input class=" form-control" id="book_name" maxlength="255" name="book_name" type="text" value="<?php echo(isset($result['book_name']) ? $result['book_name'] : ''); ?>" required>
            </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2 col-lg-2 " for="id_publisher">出版社</label>
                <div class=" col-sm-10 col-lg-10 ">
                    <input class=" form-control" id="publish_name" maxlength="255" name="publish_name" type="text" value="<?php echo(isset($result['publish_name']) ? $result['publish_name'] : ''); ?>" required>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2 col-lg-2 " for="id_page">ページ数</label>
                <div class=" col-sm-10 col-lg-10 ">
                    <input class=" form-control" id="page_num" name="page_num" type="number" value="<?php echo(isset($result['page_num']) ? $result['page_num'] : '0'); ?>" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
<?php if($mode === 'add'){ ?>
                    <button type="button" class="btn btn-primary" onClick="goSave();">登録</button>
<?php }elseif($mode === 'edit'){ ?>
                    <button type="button" class="btn btn-primary" onClick="goSave();">保存</button>
<?php } ?>
                </div>
            </div>
            <a href="/list/" class="btn btn-default btn-sm">戻る</a>
        </div>
        <input type="hidden" name="mode" value="<?php echo($mode); ?>">
    </form>
</body>
</html>