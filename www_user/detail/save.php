<?php
//他のPHPプログラムファイルの読み込み
require_once '../../lib/init.php';
//定義済みの関数を呼び出し
//設定ファイル情報を読み込む
$config = getConfig();
//セッションの初期処理
initSession($config['SESSION_DIR']);
if(!checkSessionAuth($config['SESSION_NEED_KEY'])){
    //認証されていない
    header('Location: /?err=2');
    exit;
}
if(validatePost()){
    //長い処理は、改行を入れても大丈夫。
    $dbDNS  = getDNSvalue($config['DB_TYPE'], $config['DB_NAME'],
            $config['DB_HOST'], $config['DB_PORT']);
    //PDO DBというDB操作するモジュールを利用する。
    $db     = getConnection($dbDNS, $config['DB_USER'], $config['DB_PASSWD']);

    if($_POST['mode'] === 'add'){
        $book_id = getBookIdForInsert();
        insertBooktbl($book_id);
    }elseif($_POST['mode'] === 'edit'){
        updateBooktbl();
    }else{}
    //処理が終わったので、一覧ページへ戻る
    header('Location: /list/');
}else{
    //下記のエラーページ表示
}
/**
 * 入力チェック
 * @return boolean
 */
function validatePost()
{
    $result = false;

    //book_nameがpostされていない。
    if(!isset($_POST['book_name']) || $_POST['book_name'] ===''){
        return false;
    }

    //publisher_nameがpostされていない。
    if(!isset($_POST['publish_name']) || $_POST['publish_name'] ===''){
        return false;
    }
    //page_numがpostされていない。
    if(!isset($_POST['page_num']) || $_POST['page_num'] ===''){
        return false;
    }
    //modeがpostされていない。
    if(!isset($_POST['mode']) || $_POST['mode'] === ''){
        if($_POST['mode'] === 'edit'){
            //編集モードの場合は、book_idが必要
            if(!isset($_POST['book_id']) || $_POST['book_id'] === ''){
                return false;
            }
        }
        return false;
    }
    $result = true;

    return $result;
}
/**
 * 登録用にbook_idを取得する。
 * @return number
 */
function getBookIdForInsert()
{
    global $db;

    $sql = "select count(*) as cnt from book_tbl";
    $bind = array();
    $stmt = executeSQL($sql,$bind,$db);
    $result = fetchRow($stmt);

    $id = intval($result['cnt']) + 1;

    return $id;
}
/**
 * book_tblへの登録
 * @param unknown $book_id
 */
function insertBooktbl($book_id)
{
    global $db;

    $sql = "insert into book_tbl
 (book_id,book_name,publish_name,page_num,avail_flg,insert_time,insert_id)
 values
 (:book_id,:book_name,:publish_name,:page_num,:avail_flg,:insert_time,
:insert_id)";

    $bind = array(
        ':book_id'     =>$book_id,
        ':book_name'   =>$_POST['book_name'],
        ':publish_name'=>$_POST['publish_name'],
        ':page_num'    =>$_POST['page_num'],
        ':avail_flg'   =>'1',
        ':insert_time' =>date('YmdHis'),
        ':insert_id'   =>$_SESSION['user_id']
    );
    $stmt = executeSQL($sql, $bind, $db);
}
/**
 * book_tblを更新する。
 */
function updateBooktbl()
{
    global $db;

    $sql = "update book_tbl set
 book_name = :book_name,publish_name = :publish_name,
page_num = :page_num,update_time = :update_time,update_id = :update_id
 where book_id =:book_id";

    $bind = array(
        ':book_name'   =>$_POST['book_name'],
        ':publish_name'=>$_POST['publish_name'],
        ':page_num'    =>$_POST['page_num'],
        ':update_time' =>date('YmdHis'),
        ':update_id'   =>$_SESSION['user_id'],
        ':book_id'     =>$_POST['book_id']
    );
    $stmt = executeSQL($sql, $bind, $db);
}
/**
 * POSTされた値からhiddenを作る
 * @return string
 */
function createInputForPost(){
    $line = '';
    foreach($_POST as $key => $value){
        $line .= '<input type="hidden" name="' . $key .
        '" value="' . $value . '">'."\n";
    }
    return $line;
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>登録エラー</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../statics/css/bootstrap.min.css" rel="stylesheet">
    <link href="../statics/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="../statics/js/jquery-3.1.1.min.js"></script>
    <script src="../statics/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    function goback()
    {
        location.href = '/list/';
    }
    </script>
</head>
<body>
    <form name="add" id="add" action="./add.php" method="post" class="form-horizontal" role="form" enctype="multipart/form-data">
        <div class="container">
            <h3 class="page-header">登録エラー</h3>
            <div class="alert alert-danger" role="alert">すべての登録内容が入力されておりません。
            再度、すべての入力項目を登録下さい。<br />
            一度、一覧ページに戻ります。
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="button" class="btn btn-primary" onClick="goback();">戻る</button>
                </div>
            </div>
        </div>
    </form>
</body>
</html>