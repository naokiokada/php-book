<?php
//他のPHPプログラムファイルの読み込み
require_once '../lib/init.php';

//定義済みの関数を呼び出し
$config = getConfig();

//送られてきたデータは信用できない。
//XSSされないようにクォートする。
if(isset($_GET['user_id'])){
    $userid = htmlspecialchars($_GET['user_id'], ENT_QUOTES, 'UTF-8');
}else{
    $userid = null;
}
$errMsg = detectError();
/**
 * エラーメッセージを取得する
 * @return string|boolean
 */
function detectError()
{
    $errList = getErrorCode();
    if(isset($_GET['err'])){
        $errcode = strval(intval($_GET));
        if(isset($errList[$errcode])){
            return $errList[$errcode];
        }
    }
    return false;
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>PHP-Book ログイン</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="./statics/css/bootstrap.min.css" rel="stylesheet">
    <link href="./statics/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="./statics/js/jquery-3.1.1.min.js"></script>
    <script src="./statics/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        function goSubmit()
        {
            var id = document.login.user_id.value;
            var pass = document.login.passwd.value;
            if(id && pass){
                document.login.action = './login.php';
                document.login.method = 'post';
                document.login.submit();
            }else{
                alert('IDとパスワードを入力して下さい。');
            }
        }
    </script>
</head>
<body>
    <form name="login" id="login" method="post" class="form-horizontal" role="form" enctype="multipart/form-data">
        <div class="container">
            <h3 class="page-header">PHP-Book ログイン</h3>
<?php if($errMsg){ ?>
            <div class="alert alert-danger" role="alert"><?php echo($errMsg); ?></div>
<?php } ?>
            <div class="form-group">
                <label class="control-label col-sm-2 col-lg-2 " for="id_name">ユーザーID</label>
            <div class=" col-sm-10 col-lg-10 ">
                <input class=" form-control" id="user_id" maxlength="255" name="user_id" type="text" value="<?php if($userid){ echo($userid); } ?>">
            </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2 col-lg-2 " for="id_publisher">パスワード</label>
                <div class=" col-sm-10 col-lg-10 ">
                    <input class=" form-control" id="passwd" name="passwd" type="password">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="button" class="btn btn-primary" name="btn" id="btn" onClick="goSubmit();">ログイン</button>
                </div>
            </div>
        </div>
    </form>
</body>
</html>