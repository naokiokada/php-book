<?php
//他のPHPプログラムファイルの読み込み
require_once '../../lib/init.php';

//ページ制御に必要な定数
//1ページに表示する件数
define('PAGE_COUNT', 10);
//現在ページより前のページに表示する最大ページ数
define('FRONT_PAGE_COUNT', 5);
//現在ページより次のページに表示する最大ページ数
define('BACK_PAGE_COUNT', 4);

//定義済みの関数を呼び出し
$config = getConfig();
//セッションの初期処理
initSession($config['SESSION_DIR']);
if(!checkSessionAuth($config['SESSION_NEED_KEY'])){
    //認証されていない
    header('Location: /?err=2');
    exit;
}
//長い処理は、改行を入れても大丈夫。
$dbDNS  = getDNSvalue($config['DB_TYPE'], $config['DB_NAME'],
        $config['DB_HOST'], $config['DB_PORT']);
//PDO DBというDB操作するモジュールを利用する。
$db     = getConnection($dbDNS, $config['DB_USER'], $config['DB_PASSWD']);
//ページ制御ここから開始
#全登録本の件数
$book_count = intval(getBookCount());
#全ページ数取得
$max_page   = ceil($book_count / PAGE_COUNT);
#現在のページ数(0ページより大きかったら）
#0ページより小さい場合は、1にする。
$page = 1;
if(isset($_GET['page'])){
    if(intval($_GET['page']) > 0){
        $page = intval($_GET['page']);
    }
}
#ページ数が最大値より超えている場合は1
if($page > $max_page){
    $page = 1;
}
$prev_diff = 0;
if($page - FRONT_PAGE_COUNT < 1){
    $prev_diff = FRONT_PAGE_COUNT - $page + 1;
}
$next_diff = 0;
if($page + BACK_PAGE_COUNT > $max_page){
    $next_diff = $page + BACK_PAGE_COUNT - $max_page;
}
//本リストの取得
//$result = getBookList();
$result = getBookListPage(PAGE_COUNT, $page);
/**
 * 登録済みの本リストを取得する
 * @return array
 */
function getBookList()
{
    global $db;

    $sql = "select book_id,book_name,publish_name,page_num
 from book_tbl
 where avail_flg = '1'
 order by book_id asc";
    $bind = array();

    $stmt = executeSQL($sql, $bind, $db);
    $result = fetchAll($stmt);

    return $result;
}
/**
 * 登録済みの本リストを取得
 * ページ指定する
 *
 * @param integer $count
 * @param integer $page
 * @return array
 */
function getBookListPage(int $count, int $page)
{
    global $db;

    //読み飛ばすページを算出
    $offset = ($page - 1) * $count;

    $sql = "select book_id,book_name,publish_name,
page_num from book_tbl
 where avail_flg = '1' order by book_id asc
 limit :limit offset :offset";
    $bind = array(
        ':limit'=>$count,
        ':offset'=>$offset
    );

    $stmt   = executeSQL($sql, $bind, $db);
    $result = fetchAll($stmt);

    return $result;
}
/**
 * 登録済みの本件数を数える
 * @return mixed
 */
function getBookCount()
{
    global $db;

    $sql = "select count(*) as cnt from book_tbl where avail_flg = '1'";
    $bind = array();
    $stmt = executeSQL($sql, $bind, $db);
    $result = fetchRow($stmt);
    return $result['cnt'];
}
/**
 * 以下は、リストhtmlを表示
 */
?>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>書籍の一覧</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../statics/css/bootstrap.min.css" rel="stylesheet">
    <link href="../statics/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="../statics/js/jquery-3.1.1.min.js"></script>
    <script src="../statics/js/bootstrap.min.js"></script>
    <style>
    table {
        margin-top: 8px;
    }
    </style>
    <script type="text/javascript">
        function goDelete(book_id, book_name){
            if(confirm(book_name + 'を削除しますか？')){
                document.list.action = '/detail/delete.php';
                document.list.method = 'post';
                document.list.book_id.value = book_id;
                document.list.submit();
            }
        }
    </script>
</head>
<body>
    <form name="list" id="list" action="" method="" enctype="multipart/form-data">
        <input type="hidden" name="book_id" id="book_id" value="">
        <div class="container">
            <h3 class="page-header">書籍の一覧</h3>
<?php
//追加リンクを/detail/form.phpに変更する。
#?mode=addという引数を付けておき、$_GETで取得できるようにする。
?>
            <a href="/detail/form.php?mode=add" class="btn btn-default btn-sm">追加</a>
            <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>書籍名</th>
                    <th>出版社</th>
                    <th>ページ数</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>

<?php
//$resultは2次元配列。配列の中に配列が入っている。
//foreachで配列の各要素にアクセス。
foreach($result as $row){
?>
                <tr>
                    <td><?php echo($row['book_id']); ?></td>
                    <td><?php echo($row['book_name']); ?></td>
                    <td><?php echo($row['publish_name']); ?></td>
                    <td><?php echo($row['page_num']); ?></td>
                    <td>
                        <a href="../detail/form.php?mode=edit&book_id=<?php echo($row['book_id']); ?>" class="btn btn-default btn-sm">修正</a>
                        <a href="javascript:goDelete('<?php echo($row['book_id']); ?>','<?php echo($row['book_name']); ?>');" btn btn-default btn-sm">削除</a>
                    </td>
                </tr>
<?php
}
?>
            </tbody>
            </table>
            <ul class="pagination">
<?php if($page <= 1){
    //１ページ目の場合、前のページ＜＜は利用できない。
?>
                <li class="disabled"><a href="#">«</a></li>
<?php
    }else{
    //それ以外のページは＜＜を表示する。
?>
                <li class=""><a href="./?page=<?php echo($page - 1); ?>">«</a></li>
<?php
    }
//現在のページより前へ減らして行く
for($i = FRONT_PAGE_COUNT + $next_diff; $i > 0; $i--){
    if($page - $i < 1){
        continue;
    }else{
?>
                <li><a href="./?page=<?php echo($page - $i); ?>&-<?php echo($i); ?>"><?php echo($page - $i); ?></a></li>
<?php
    }
}
//現在のページを表示
?>
                <li class="active"><a href="./?page=<?php echo($page); ?>"><?php echo($page); ?></a></li>
<?php
//次のページへ増やしていく
for($i = 1; $i <= BACK_PAGE_COUNT + $prev_diff; $i++){
    if($page + $i > $max_page){
        break;
    }else{
?>
                <li><a href="./?page=<?php echo($i + $page); ?>&+<?php echo($i); ?>"><?php echo($i + $page); ?></a></li>
<?php
    }
}
if($page >= $max_page){
    //ページが最大ページ以上
?>
                <li><a class="disabled" href="#">»</a></li>
<?php
    }else{
?>
                <li><a class="" href="./?page=<?php echo($page + 1); ?>">»</a></li>
<?php
    }
?>
            </ul>
        </div>
    </form>
</body>
</html>