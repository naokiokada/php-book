<?php
if(isset($_GET['page'])){
    $page = intval($_GET['page']);
}else{
    $page = 1;
}
if($page < 1){
    $page = 1;
}
$count = 200;
$pageCnt = 10;
$maxpage = ceil($count / $pageCnt);

$front = 5;
$back  = 4;

echo('page:'.$page);

$prevDiff = 0;
if($page - $front < 1){
    $prevDiff = $front - $page + 1;
}
$nextDiff = 0;
if($page + $back > $maxpage){
    $nextDiff = $page + $back - $maxpage;
}
echo("\n");
echo('pd:'.$prevDiff);
echo("\n");
echo('nd:'.$nextDiff);
?>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>書籍の一覧</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../statics/css/bootstrap.min.css" rel="stylesheet">
    <link href="../statics/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="../statics/js/jquery-3.1.1.min.js"></script>
    <script src="../statics/js/bootstrap.min.js"></script>
    <style>
    table {
        margin-top: 8px;
    }
    </style>
</head>
<body>
    <form name="list" id="list" action="./add.php" method="post" enctype="multipart/form-data">
        <div class="container">
            <h3 class="page-header">書籍の一覧</h3>
            <a href="./add.php" class="btn btn-default btn-sm">追加</a>
            <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>書籍名</th>
                    <th>出版社</th>
                    <th>ページ数</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            </table>
            <ul class="pagination">
<?php if($page <= 1){
    //１ページ目の場合、前のページ＜＜は表示しない。
?>
                <li class="disabled"><a href="#">«</a></li>
<?php }else{
    //それ以外のページは＜＜を表示する。
?>
                <li class=""><a href="./pagetest.php?page=<?php echo($page - 1); ?>">«</a></li>
<?php } ?>

<?php
//現在のページより前へ減らして行く
for($i = $front + $nextDiff; $i > 0; $i--){
    if($page - $i < 1){
        continue;
?>
<?php
    }else{
?>
                <li><a href="./pagetest.php?page=<?php echo($page - $i); ?>&-<?php echo($i); ?>"><?php echo($page - $i); ?></a></li>
<?php
    }
}
?>
<?php
//現在のページを表示
?>
<li class="active"><a href="./pagetest.php?page=<?php echo($page); ?>"><?php echo($page); ?></a></li>
<?php
//前へ増やしていく
for($i = 1; $i <= $back + $prevDiff; $i++){
    if($page + $i > $maxpage){
        break;
?>
<?php }else{
?>
                <li><a href="./pagetest.php?page=<?php echo($i + $page); ?>&+<?php echo($i); ?>"><?php echo($i + $page); ?></a></li>
<?php
      }
}
?>
<?php if($page >= $maxpage){
    //ページが最大ページ以上
?>
                <li><a class="disabled" href="#">»</a></li>
<?php }else{ ?>
                <li><a class="" href="./pagetest.php?page=<?php echo($page + 1); ?>">»</a></li>
<?php } ?>
            </ul>
        </div>
    </form>
</body>
</html>