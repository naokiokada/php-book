<?php
//他のPHPプログラムファイルの読み込み
require_once '../lib/init.php';

//定義済みの関数を呼び出し
$config = getConfig();

//長い処理は、改行を入れても大丈夫。
$dbDNS  = getDNSvalue($config['DB_TYPE'], $config['DB_NAME'],
    $config['DB_HOST'], $config['DB_PORT']);

//PDO DBというDB操作するモジュールを利用する。
$db     = getConnection($dbDNS, $config['DB_USER'], $config['DB_PASSWD']);
//POSTでログイン画面に入力されたuseridとpasswdの値を取得する。
//グローバル変数と呼ぶ。
$userid = $_POST['user_id'];
$passwd = $_POST['passwd'];

if(checkUserAuth()){
    //認証後→/list/index.phpへ移動する。
    updateSessionForAuth();
    header('Location: ./list/');
}else{
    //認証失敗→index.phpに戻る。err=1というGETパラメーター指定
    header('Location: /?err=1&user_id='.$_POST['user_id']);
}
/**
 * メンバー認証をチェックする。
 * @return boolean
 */
function checkUserAuth(){
    global $db,$userid,$passwd;

    $sql = "select user_id from user_tbl where
 user_id = :user_id and passwd =:passwd and avail_flg = '1'";

    $bind = array(
            ':user_id' => $userid,
            ':passwd' => $passwd
    );
    $stmt = executeSQL($sql, $bind, $db);
    $result = fetchRow($stmt);

    if(isset($result['user_id'])){
        if($result['user_id'] === $userid){
            return true;
        }
    }
    return false;
}
/**
 * 認証完了時にセッションを更新する。
 */
function updateSessionForAuth()
{
    global $config,$userid;

    //セッションの初期処理
    initSession($config['SESSION_DIR']);
    setSessionAuth($config['SESSION_NEED_KEY'], $userid);
}