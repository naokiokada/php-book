<?php
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>基本的なフォーム</title>
<script type="text/javascript">
    function kakunin(){
        var input1 = document.fm.input1.value;
        alert(input1);
    }
    function goSubmit(){
        var input1 = document.fm.input1.value;
        if(input1){
            //document.fm.method = 'get';
            document.fm.method = 'post';
            document.fm.action = './submit.php';
            document.fm.submit();
        }else{
            alert('入力されていません。');
        }
    }
</script>
</head>
<body>
<h1>基本的な画面遷移、フォーム操作（submit）の練習です。</h1>
<form name="fm" method="post" action="./submit.php"/>
リンクでアクセス<br />
リンクは、<a href="./submit.php?input1=あいうえお&textarea1=abcde">こちら</a><br /><br />
textタイプ
<input type="text" name="input1" value=""><br /><br />
ラジオボタン<br />
<input type="radio" name="input2" value="あり">あり&nbsp;&nbsp;
<input type="radio" name="input2" value="なし">なし<br /><br />
チェックボックス<br />
<input type="checkbox" name="input3[]" value="左">左&nbsp;
<input type="checkbox" name="input3[]" value="中">中&nbsp;
<input type="checkbox" name="input3[]" value="右">右<br /><br />
セレクトボックス<br />
<select name="select1">
    <option></option>
    <option>選択１</option>
    <option>選択２</option>
    <option>選択３</option>
    <option>選択４</option>
</select><br /><br />
テキストエリア<br />
<textarea name="textarea1" rows="4" cols="100">
</textarea><br /><br />
通常ボタン：javascriptの起動に使われる。<br />
<button type="button" name="btn" id="btn" onClick="goSubmit();">通常ボタン</button><br />
submitボタン<br />
<input type="submit" value="サブミット"/><br />
リセット<br />
<input type="reset" value="リセット"/>
<input type="hidden" name="hidden_key" value="これは非表示の項目ですよ！！">
</form>
</body>
</html>