<?php
$request = array();
//GETで受け取った情報
//$request = $_GET;
//POSTで受け取った情報
$request = $_POST;
//var_dump($request);
//exit;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>基本的なフォーム</title>
<script type="text/javascript">
</script>
</head>
<body>
<h1>submit内容確認画面</h1>
<form name="submit" method="" action=""/>
<table border="1">
    <tr>
        <th>フォームの入力項目のname（キー）</th>
        <th>入力・選択した値</th>
    </tr>
<?php
//連想配列の各要素にアクセス。キーと値部分に分ける。
foreach($request as $key => $val){
?>
    <tr>
        <td><?php echo($key); ?></td>
        <td>
<?php
if($key === 'input3'){
    //チェックボックスは、値が配列になるので、連結する。
    $val = implode(',', $val);
}
    echo(htmlspecialchars($val));
?>
        </td>
        <input type="hidden" name="<?php echo($key); ?>" value="<?php echo(htmlspecialchars($val)); ?>">
</tr>
<?php
}
?>
</table>
</form>
</body>
</html>